//
//  AnimalModel.swift
//  EsameAlessandroCignolini
//
//  Created by alessandro cignolini on 19/12/18.
//  Copyright © 2018 alessandro cignolini. All rights reserved.
//

import Foundation
protocol AnimalProtocol {
    func animalRetrived(animal:[Animal])
    func animalUrlError()


}
class AnimalModel {
    var delegate:AnimalProtocol?
    func getAnimal() {
        getRemoteJsonFile()
    }
    
    
    //prende i dati da remoto
    func getRemoteJsonFile() {
        let stringUrl = "http://www.willyx.it/esercizio/animaliStrani.json"
        let url = URL(string: stringUrl)
        guard url != nil else{
            print("couldn't get a URL object")
            return
        }
        let session = URLSession.shared
        let dataTask = session.dataTask(with: url!) { (data, response, error) in
            if error == nil && data != nil{
                let decoder = JSONDecoder()
                do{
                    let arrray = try decoder.decode([Animal].self, from: data!)
                    DispatchQueue.main.async {
                        self.delegate?.animalRetrived(animal: arrray)
                    }
                }
                catch{
                    print("couldn't parse the json")
                    self.delegate?.animalUrlError()
                }
            }
        }
        dataTask.resume()
    }
}
