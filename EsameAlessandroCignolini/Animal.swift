//
//  Animal.swift
//  EsameAlessandroCignolini
//
//  Created by alessandro cignolini on 19/12/18.
//  Copyright © 2018 alessandro cignolini. All rights reserved.
//

import Foundation
struct Animal: Codable {
    var nome : String?
    var energiaMax : Int?
    var vola : Bool?
    var immagini : [String]?
}
