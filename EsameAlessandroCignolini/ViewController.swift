//
//  ViewController.swift
//  EsameAlessandroCignolini
//
//  Created by alessandro cignolini on 19/12/18.
//  Copyright © 2018 alessandro cignolini. All rights reserved.
//

import UIKit

class ViewController: UIViewController, AnimalProtocol {
    @IBOutlet var buttonEnergy: UIButton!
    @IBOutlet var leadingConstraint: NSLayoutConstraint!
    @IBOutlet var trailingConstraint: NSLayoutConstraint!
    @IBOutlet var energy: UILabel!
    @IBOutlet var animalImage: UIImageView!
    @IBOutlet var animalName: UILabel!
    @IBOutlet var animalFly: UILabel!
    var vita = 0
    var model = AnimalModel()
    var animal = [Animal]()
    var timer : Timer?
    var randomIndex = 0
   
    override func viewDidLoad() {
        super.viewDidLoad()
        model.delegate = self
        model.getAnimal()
       
    }
    
    //cambia personaggio random
    @IBAction func Reset(_ sender: Any) {
        timer?.invalidate()
        getAniaml()
        slideInImage()
        buttonEnergy.isEnabled = true
        
        
    }
    //aumenta la vita e se la vita è al massimo non aumenta
    @IBAction func AddEnergy(_ sender: Any) {
        vita += 2
        if vita > animal[randomIndex].energiaMax! {
            vita = animal[randomIndex].energiaMax!
        }
        upDateVita()
        }
       
    
    //metodo che aggiorna la vita per visualizzarla correttamente
    func upDateVita()  {
        energy.text = "vita: \(vita) / \(animal[randomIndex].energiaMax! )"
    }
    
    func animalRetrived(animal: [Animal]) {
        self.animal = animal
        getAniaml()
        print("file restituito")
    }
    
    //metodo che mi da l'animale random con tutti i suoi valori
    func getAniaml()  {
        randomIndex = Int(arc4random_uniform(UInt32(animal.count)))
        vita = animal[randomIndex].energiaMax!
        animalName.text = "\(animal[randomIndex].nome!)"
        if animal[randomIndex].vola!{
            animalFly.text = " Sa volare"
        }else{
            animalFly.text = " non sa volare"
        }
        animalImage.load(URL(string:"http://www.willyx.it/esercizio/\(animal[randomIndex].immagini![Int(arc4random_uniform(UInt32(Int((animal[randomIndex].immagini?.count)!))))])")!);
        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(timerElapsed), userInfo: nil, repeats: true)
        RunLoop.main.add(timer!, forMode: .common)
    }
    func animalUrlError() {
        print("error")
    }
    
    //serve per far diminuire la vita della creatura
   @objc func timerElapsed(){
        vita -= 1
        if vita < 0 {
            vita = 0
            timer?.invalidate()
            buttonEnergy.isEnabled = false
            slideOutImage(url: "http://www.willyx.it/esercizio/imgs/rip.jpg")
            if animal[randomIndex].vola!{
                animalFly.text = "Vola tra gli angeli"
            }else{
                animalFly.text = "Marcisce sul terreno"
            }
           
            
            return;
        }
   
        upDateVita()
    }
    
    
    
    
    //questi due metodi servono per l'animazione dell'immagine quando ho la creatura viene generata o quando muore
    func slideInImage() {
        leadingConstraint.constant = 1000
        trailingConstraint.constant = -1000
        view.layoutIfNeeded()
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
            self.leadingConstraint.constant = 0
            self.trailingConstraint.constant = 0
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    func slideOutImage(url: String){
        leadingConstraint.constant = 0
        trailingConstraint.constant = 0
        view.layoutIfNeeded()
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
            self.leadingConstraint.constant = -1000
            self.trailingConstraint.constant = 1000
            self.view.layoutIfNeeded()
        }) { (completed) in
            self.animalImage.load(URL(string: url)!)
            self.slideInImage()
        }
        
    }
    
}

extension UIImageView {
    func load(_ url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}


